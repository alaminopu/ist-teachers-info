<?php
require 'vendor/autoload.php';

$app = new \Slim\Slim();

$app->config(
	array(
		'debug' => true,
		'templates.path' => './templates',
		'mode'=>'development'
	)
);

$app->get('/hello/:name', function ($name) use ($app) {
    //$dap = Slim::getInstance();
    $app->render('test.php');
});


$app->run();